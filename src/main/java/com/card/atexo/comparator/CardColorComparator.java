package com.card.atexo.comparator;

import com.card.atexo.model.CardColor;
import lombok.Data;

import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Data
public class CardColorComparator implements Comparator<CardColor> {

    private final EnumMap<CardColor, Integer> cardColorOrder;

    public static CardColorComparator createRandomComparator() {
        List<Integer> integers = IntStream.range(0, CardColor.values().length).boxed().collect(Collectors.toList());
        Collections.shuffle(integers);

        EnumMap<CardColor, Integer> randomOrder = new EnumMap<>(CardColor.class);
        CardColor[] cardColors = CardColor.values();
        for (int i = 0; i < cardColors.length; ++i) {
            randomOrder.put(cardColors[i], integers.get(i));
        }

        return new CardColorComparator(randomOrder);
    }

    @Override
    public int compare(CardColor cardColor1, CardColor cardColor2) {
        return this.cardColorOrder.get(cardColor1) - this.cardColorOrder.get(cardColor2);
    }
}
