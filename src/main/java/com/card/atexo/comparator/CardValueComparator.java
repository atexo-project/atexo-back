package com.card.atexo.comparator;

import com.card.atexo.model.CardValue;
import lombok.Data;

import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Data
public class CardValueComparator implements Comparator<CardValue> {
    private final EnumMap<CardValue, Integer> cardValueOrder;


    public static CardValueComparator createRandomComparator() {
        List<Integer> integers = IntStream.range(0, CardValue.values().length).boxed().collect(Collectors.toList());
        Collections.shuffle(integers);

        EnumMap<CardValue, Integer> randomOrder = new EnumMap<>(CardValue.class);
        CardValue[] cardValues = CardValue.values();
        for (int i = 0; i < cardValues.length; ++i) {
            randomOrder.put(cardValues[i], integers.get(i));
        }

        return new CardValueComparator(randomOrder);
    }

    @Override
    public int compare(CardValue cardValue1, CardValue cardValue2) {
        return this.cardValueOrder.get(cardValue1) - this.cardValueOrder.get(cardValue2);
    }
}
