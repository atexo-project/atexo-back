package com.card.atexo.comparator;

import com.card.atexo.model.Card;
import lombok.AllArgsConstructor;

import java.util.Comparator;

@AllArgsConstructor
public class CardComparator implements Comparator<Card> {
    private final CardColorComparator cardColorComparator;
    private final CardValueComparator cardValueComparator;

    @Override
    public int compare(Card card1, Card card2) {
        int comparedColor = this.cardColorComparator.compare(card1.getCardColor(), card2.getCardColor());
        if(0 != comparedColor){
            return comparedColor;
        }
        return this.cardValueComparator.compare(card1.getCardValue(), card2.getCardValue());
    }
}
