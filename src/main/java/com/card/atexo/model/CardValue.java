package com.card.atexo.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum CardValue {
    ACE("A"),
    KING("K"),
    QUEEN("Q"),
    JACK("J"),
    TEN("10"),
    NINE("9"),
    EIGHT("8"),
    SEVEN("7"),
    SIX("6"),
    FIVE("5"),
    FOUR("4"),
    THREE("3"),
    TWO("2");

    private final String value;

    CardValue(String value) {
        this.value = value;
    }

    @JsonValue
    public String toString(){
        return this.value;
    }
}
