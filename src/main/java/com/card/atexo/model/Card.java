package com.card.atexo.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;

@Data
public class Card {
    private final CardValue cardValue;
    private final CardColor cardColor;

    @JsonValue
    @Override
    public String toString(){
        return this.cardValue.toString() + this.cardColor.toString();
    }
}
