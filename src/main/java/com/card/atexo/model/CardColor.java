package com.card.atexo.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum CardColor {
    SPADE("S"),
    DIAMOND("D"),
    CLUB("C"),
    HEART("H");

    private final String color;

    CardColor(String color) {
        this.color = color;
    }

    @JsonValue
    public String toString(){
        return this.color;
    }
}
