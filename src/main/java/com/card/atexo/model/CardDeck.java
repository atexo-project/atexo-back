package com.card.atexo.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CardDeck {
    public static final int CARDS_SIZE = 52;
    private static final List<Card> cards = createCards();

    private static List<Card> createCards() {
        List<Card> cards = new ArrayList<>(CARDS_SIZE);
        for (CardValue cardValue : CardValue.values()) {
            for (CardColor cardColor : CardColor.values()) {
                cards.add(new Card(cardValue, cardColor));
            }
        }
        return cards;
    }

    public static CardHand drawRandom(int count){
        List<Integer> integers = IntStream.range(0, CARDS_SIZE).boxed().collect(Collectors.toList());
        Collections.shuffle(integers);
        
        List<Card> randomCardList = integers.stream().limit(count).map(x -> cards.get(x)).toList();
        return new CardHand(randomCardList);
    }

}
