package com.card.atexo.model;

import com.card.atexo.comparator.CardComparator;
import lombok.Data;

import java.util.List;

@Data
public class CardHand {
    private final List<Card> cards;

    public boolean isOrderd(CardComparator cardComparator) {
        return this.cards.stream().sorted(cardComparator).toList().equals(this.cards);
    }

    public CardHand getOrderedCardHand(CardComparator cardComparator) {
        List<Card> cardList = this.cards.stream().sorted(cardComparator).toList();
        return new CardHand(cardList);
    }

    @Override
    public String toString(){
        return this.cards.toString();
    }
}
