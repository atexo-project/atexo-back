package com.card.atexo.controler;

import com.card.atexo.model.CardColor;
import com.card.atexo.model.CardDeck;
import com.card.atexo.model.CardHand;
import com.card.atexo.model.CardValue;
import com.card.atexo.comparator.CardColorComparator;
import com.card.atexo.comparator.CardComparator;
import com.card.atexo.comparator.CardValueComparator;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;


@CrossOrigin(origins = "http://127.0.0.1:5173", methods = {RequestMethod.GET, RequestMethod.POST})
@RestController
@RequestMapping("cards")
public class CardControler {
    private CardColorComparator cardColorComparator = CardColorComparator.createRandomComparator();
    private CardValueComparator cardValueComparator = CardValueComparator.createRandomComparator();
    private CardComparator cardComparator = new CardComparator(this.cardColorComparator, this.cardValueComparator);
    private CardHand lastDrawnHand;

    @GetMapping(value = "/draw", produces = "application/json")
    public CardHand drawCards(){
        this.lastDrawnHand = CardDeck.drawRandom(10);
        return this.lastDrawnHand;
    }

    @GetMapping(value = "/last-hand-ordered", produces = "application/json")
    public CardHand getLastDrawnHandOrdered(){
        return this.lastDrawnHand.getOrderedCardHand(this.cardComparator);
    }

    @GetMapping(value = "/color-order", produces = "application/json")
    public List<CardColor> getCardColorOrder(){
        EnumMap<CardColor, Integer> cardColorOrder = this.cardColorComparator.getCardColorOrder();
        return cardColorOrder.entrySet()
                             .stream()
                             .sorted(Comparator.comparingInt(Map.Entry::getValue))
                             .map(Map.Entry::getKey)
                             .toList();
    }

    @GetMapping(value = "/value-order", produces = "application/json")
    public List<CardValue> getCardValueOrder(){
        EnumMap<CardValue, Integer> cardValueOrder = this.cardValueComparator.getCardValueOrder();
        return cardValueOrder.entrySet()
                             .stream()
                             .sorted(Comparator.comparingInt(Map.Entry::getValue))
                             .map(Map.Entry::getKey)
                             .toList();
    }

    @PostMapping(value = "random-order", produces = "application/json")
    public void randomOrder(){
        this.cardColorComparator = CardColorComparator.createRandomComparator();
        this.cardValueComparator = CardValueComparator.createRandomComparator();
        this.cardComparator = new CardComparator(this.cardColorComparator, this.cardValueComparator);
    }

}
